import SwiftUI

@main
struct MinimalSwistUIProjectApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
